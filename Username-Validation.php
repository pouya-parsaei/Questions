<!-- Codeland Username Validation
Have the function CodelandUsernameValidation(str) take the str parameter being passed and determine if the string is a valid username according to the following rules:

1. The username is between 4 and 25 characters.
2. It must start with a letter.
3. It can only contain letters, numbers, and the underscore character.
4. It cannot end with an underscore character.

If the username is valid then your program should return the string true, otherwise return the string false.
Examples
Input: "aa_"
Output: false
Input: "u__hello_world123"
Output: true -->
<?php

function CodelandUsernameValidation(string $str = "pouya_", int $min = 4, int $max = 25)
{
    $len = strlen($str);
    if ($len < $min || $len > $max) {
        return 'false1';
    } elseif (!preg_match('/^[a-zA-Z]/', $str)) {
        return 'false2';
    } elseif (!preg_match('/^[a-zA-Z0-9_]+$/', $str)) {
        return 'false3';
    } elseif (preg_match('/_$/', $str)) {
        return 'false4';
    }
    return 'true';
}

echo CodelandUsernameValidation();
// keep this function call here  
// echo CodelandUsernameValidation(fgets(fopen('php://stdin', 'r')));

?>